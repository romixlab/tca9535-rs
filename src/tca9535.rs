use super::Address;
use embedded_hal::blocking::i2c::{Write, WriteRead};
use core::marker::PhantomData;

bitflags::bitflags! {
    pub struct Port: u16 {
        const P00 = 0b0000_0000_0000_0001;
        const P01 = 0b0000_0000_0000_0010;
        const P02 = 0b0000_0000_0000_0100;
        const P03 = 0b0000_0000_0000_1000;
        const P04 = 0b0000_0000_0001_0000;
        const P05 = 0b0000_0000_0010_0000;
        const P06 = 0b0000_0000_0100_0000;
        const P07 = 0b0000_0000_1000_0000;

        const P10 = 0b0000_0001_0000_0000;
        const P11 = 0b0000_0010_0000_0000;
        const P12 = 0b0000_0100_0000_0000;
        const P13 = 0b0000_1000_0000_0000;
        const P14 = 0b0001_0000_0000_0000;
        const P15 = 0b0010_0000_0000_0000;
        const P16 = 0b0100_0000_0000_0000;
        const P17 = 0b1000_0000_0000_0000;
    }
}
pub struct Tca9535<T> {
    address: Address,
    i2c: PhantomData<T>,
}

impl<T, E> Tca9535<T>
    where
        T: WriteRead<Error = E> + Write<Error = E>,
{
    pub fn new(_i2c: &T, address: Address) -> Self {
        Self {
            address,
            i2c: PhantomData,
        }
    }

    pub fn address(&self) -> Address {
        self.address
    }

    /// Read a pair of registers.
    ///
    /// The TCA9535 registers come in pairs (e.g. CONFIG0, CONFIG1) corresponding
    /// to the two 8-port outputs (CONFIG0 for Port 0, CONFIG1 for Port 1)
    ///
    /// Intended to be called only using the Port 0 registers and will automatically
    /// read from the matching the Port 1 register
    fn read_pair(&self, i2c: &mut T, reg: Register) -> Result<Port, E> {
        let mut buffer = [0u8; 2];
        i2c.write_read(self.address as u8, &[reg as u8], &mut buffer)
            .map(|_| unsafe { Port::from_bits_unchecked(u16::from_le_bytes(buffer)) })
    }

    /// Write a pair of registers.
    ///
    /// The TCA9535 registers come in pairs (e.g. CONFIG0, CONFIG1) corresponding
    /// to the two 8-port outputs (CONFIG0 for Port 0, CONFIG1 for Port 1)
    ///
    /// Intended to be called only using the Port 0 registers and will automatically
    /// write to the matching the Port 1 register
    fn write_pair(&self, i2c: &mut T, reg: Register, port: Port) -> Result<(), E> {
        let bytes = port.bits.to_le_bytes();
        let buffer = [reg as u8, bytes[0], bytes[1]];
        i2c.write(self.address as u8, &buffer)
    }
    /// The Input Port registers reflect the incoming logic levels of the pins, regardless of
    /// whether the pin is defined as an input or an output by the Configuration Register.
    pub fn read_inputs(&self, i2c: &mut T) -> Result<Port, E> {
        self.read_pair(i2c, Register::INPUT_PORT0)
    }

    /// The Output Port registers show the outgoing logic levels of the pins defined as outputs
    /// by the Configuration Register.  These values reflect the state of the flip-flop controlling
    /// the output section, not the actual pin value.
    pub fn read_outputs(&self, i2c: &mut T) -> Result<Port, E> {
        self.read_pair(i2c, Register::OUTPUT_PORT0)
    }

    /// Set the output state for all pins configured as output pins in the Configuration Register.
    /// Has no effect for pins configured as input pins.
    ///
    /// To clear outputs use Port::empty() or the clear_outputs() method
    pub fn write_outputs(&self, i2c: &mut T, output: Port) -> Result<(), E> {
        self.write_pair(i2c, Register::OUTPUT_PORT0, output)
    }

    /// Set all outputs low.
    ///
    /// Equivalent to calling `Tca9535::write_outputs(i2c, Port::empty())`.
    pub fn clear_outputs(&self, i2c: &mut T) -> Result<(), E> {
        self.write_pair(i2c, Register::OUTPUT_PORT0, Port::empty())
    }

    /// Configure the direction of the I/O pins.  Ports set to 1 are configured as input pins with
    /// high-impedance output drivers.  Ports set to 0 are set as output pins.
    pub fn write_config(&self, i2c: &mut T, config: Port) -> Result<(), E> {
        self.write_pair(i2c, Register::CONFIG_PORT0, config)
    }

    /// Read the direction of the I/O pins.  Ports set to 1 are configured as input pins with
    /// high-impedance output drivers.  Ports set to 0 are set as output pins.
    pub fn read_config(&self, i2c: &mut T) -> Result<Port, E> {
        self.read_pair(i2c, Register::CONFIG_PORT0)
    }

    /// The Polarity Inversion registers allow polarity inversion of pins defined as inputs by the
    /// Configuration register. If a bit in this register is set the corresponding pin's polarity
    /// is inverted. If a bit in this register is cleared, the corresponding pin's original polarity
    /// is retained.
    pub fn set_inverted(&self, i2c: &mut T, invert: Port) -> Result<(), E> {
        self.write_pair(i2c, Register::POLARITY_INVERT0, invert)
    }

    /// The Polarity Inversion registers allow polarity inversion of pins defined as inputs by the
    /// Configuration register. If a bit in this register is set the corresponding pin's polarity
    /// is inverted. If a bit in this register is cleared, the corresponding pin's original polarity
    /// is retained.
    pub fn is_inverted(&self, i2c: &mut T) -> Result<Port, E> {
        self.read_pair(i2c, Register::POLARITY_INVERT0)
    }
}

#[allow(non_camel_case_types)]
#[derive(Copy, Clone)]
pub enum Register {
    INPUT_PORT0 = 0x00,
    INPUT_PORT1 = 0x01,
    OUTPUT_PORT0 = 0x02,
    OUTPUT_PORT1 = 0x03,
    POLARITY_INVERT0 = 0x04,
    POLARITY_INVERT1 = 0x05,
    CONFIG_PORT0 = 0x06,
    CONFIG_PORT1 = 0x07,
}