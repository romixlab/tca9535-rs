//! TCA9535 Low-Voltage 16-Bit I2C and SMBus Low-Power I/O Expander
//!
//! https://www.ti.com/lit/ds/symlink/tca9535.pdf

// Tests require std for mocking the i2c bus
#![cfg_attr(not(test), no_std)]

use core::convert::TryFrom;

pub mod tca9534;
pub mod tca9535;

/// Valid addresses for the TCA9535
#[allow(non_camel_case_types)]
#[repr(u8)]
#[derive(Copy, Clone)]
pub enum Address {
    ADDR_0x20 = 0x20,
    ADDR_0x21 = 0x21,
    ADDR_0x22 = 0x22,
    ADDR_0x23 = 0x23,
    ADDR_0x24 = 0x24,
    ADDR_0x25 = 0x25,
    ADDR_0x26 = 0x26,
    ADDR_0x27 = 0x27,
}

impl TryFrom<u8> for Address {
    type Error = ();
    fn try_from(value: u8) -> Result<Self, Self::Error> {
        match value {
            0x20 => Ok(Address::ADDR_0x20),
            0x21 => Ok(Address::ADDR_0x21),
            0x22 => Ok(Address::ADDR_0x22),
            0x23 => Ok(Address::ADDR_0x23),
            0x24 => Ok(Address::ADDR_0x24),
            0x25 => Ok(Address::ADDR_0x25),
            0x26 => Ok(Address::ADDR_0x26),
            0x27 => Ok(Address::ADDR_0x27),
            _ => Err(()),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use super::tca9535::*;
    use embedded_hal_mock::i2c::{Mock, Transaction};

    #[test]
    fn test_read_inputs() {
        let addr = Address::ADDR_0x24;
        let expected_value = Port::P00 | Port::P14;
        let expected = [Transaction::write_read(
            addr as u8,
            vec![Register::INPUT_PORT0 as u8],
            expected_value.bits().to_le_bytes().to_vec(),
        )];

        let mut i2c = Mock::new(&expected);
        let device = Tca9535::new(&i2c, addr);
        let result = device.read_inputs(&mut i2c).unwrap();
        assert_eq!(result, expected_value);
    }

    #[test]
    fn test_read_empty() {
        let addr = Address::ADDR_0x24;
        let raw_response_value = vec![0, 0];
        let expected = [Transaction::write_read(
            addr as u8,
            vec![Register::INPUT_PORT0 as u8],
            raw_response_value,
        )];
        let expected_result = Port::empty();

        let mut i2c = Mock::new(&expected);
        let device = Tca9535::new(&i2c, addr);
        let result = device.read_inputs(&mut i2c).unwrap();
        assert_eq!(result, expected_result);
    }

    #[test]
    fn test_read_outputs() {
        let addr = Address::ADDR_0x22;
        let raw_response_value = vec![0xAA, 0x55];
        let expected = [Transaction::write_read(
            addr as u8,
            vec![Register::OUTPUT_PORT0 as u8],
            raw_response_value,
        )];
        let expected_result = Port::P01
            | Port::P03
            | Port::P05
            | Port::P07
            | Port::P10
            | Port::P12
            | Port::P14
            | Port::P16;

        let mut i2c = Mock::new(&expected);
        let device = Tca9535::new(&i2c, addr);
        let result = device.read_outputs(&mut i2c).unwrap();
        assert_eq!(result, expected_result);
    }
}
