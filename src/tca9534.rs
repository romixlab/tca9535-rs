use super::Address;
use embedded_hal::blocking::i2c::{Write, WriteRead};
use core::marker::PhantomData;

bitflags::bitflags! {
    pub struct Port: u8 {
        const P00 = 0b0000_0001;
        const P01 = 0b0000_0010;
        const P02 = 0b0000_0100;
        const P03 = 0b0000_1000;
        const P04 = 0b0001_0000;
        const P05 = 0b0010_0000;
        const P06 = 0b0100_0000;
        const P07 = 0b1000_0000;
    }
}
pub struct Tca9534<T> {
    address: Address,
    i2c: PhantomData<T>,
}

impl<T, E> Tca9534<T>
    where
        T: WriteRead<Error = E> + Write<Error = E>,
{
    pub fn new(_i2c: &T, address: Address) -> Self {
        Self {
            address,
            i2c: PhantomData,
        }
    }

    pub fn address(&self) -> Address {
        self.address
    }

    /// Read one register.
    ///
    fn read(&self, i2c: &mut T, reg: Register) -> Result<Port, E> {
        let mut buffer = [0u8; 1];
        i2c.write_read(self.address as u8, &[reg as u8], &mut buffer)
            .map(|_| unsafe { Port::from_bits_unchecked(u8::from_le_bytes(buffer)) })
    }

    /// Write one register.
    fn write(&self, i2c: &mut T, reg: Register, port: Port) -> Result<(), E> {
        let bytes = port.bits.to_le_bytes();
        let buffer = [reg as u8, bytes[0]];
        i2c.write(self.address as u8, &buffer)
    }
    /// The Input Port register reflect the incoming logic levels of the pins, regardless of
    /// whether the pin is defined as an input or an output by the Configuration Register.
    pub fn read_inputs(&self, i2c: &mut T) -> Result<Port, E> {
        self.read(i2c, Register::INPUT_PORT)
    }

    /// The Output Port register show the outgoing logic levels of the pins defined as outputs
    /// by the Configuration Register.  These values reflect the state of the flip-flop controlling
    /// the output section, not the actual pin value.
    pub fn read_outputs(&self, i2c: &mut T) -> Result<Port, E> {
        self.read(i2c, Register::OUTPUT_PORT)
    }

    /// Set the output state for all pins configured as output pins in the Configuration Register.
    /// Has no effect for pins configured as input pins.
    ///
    /// To clear outputs use Port::empty() or the clear_outputs() method
    pub fn write_outputs(&self, i2c: &mut T, output: Port) -> Result<(), E> {
        self.write(i2c, Register::OUTPUT_PORT, output)
    }

    pub fn modify_outputs(&self, i2c: &mut T, mask: Port, pins: Port) -> Result<(), E> {
        let outputs = self.read_outputs(i2c)?;
        self.write_outputs(i2c, (outputs & !mask) | pins)
    }

    /// Set all outputs low.
    ///
    /// Equivalent to calling `Tca9535::write_outputs(i2c, Port::empty())`.
    pub fn clear_outputs(&self, i2c: &mut T) -> Result<(), E> {
        self.write(i2c, Register::OUTPUT_PORT, Port::empty())
    }

    /// Configure the direction of the I/O pins.  Ports set to 1 are configured as input pins with
    /// high-impedance output drivers.  Ports set to 0 are set as output pins.
    pub fn write_config(&self, i2c: &mut T, config: Port) -> Result<(), E> {
        self.write(i2c, Register::CONFIG_PORT, config)
    }

    /// Read the direction of the I/O pins.  Ports set to 1 are configured as input pins with
    /// high-impedance output drivers.  Ports set to 0 are set as output pins.
    pub fn read_config(&self, i2c: &mut T) -> Result<Port, E> {
        self.read(i2c, Register::CONFIG_PORT)
    }

    /// The Polarity Inversion registers allow polarity inversion of pins defined as inputs by the
    /// Configuration register. If a bit in this register is set the corresponding pin's polarity
    /// is inverted. If a bit in this register is cleared, the corresponding pin's original polarity
    /// is retained.
    pub fn set_inverted(&self, i2c: &mut T, invert: Port) -> Result<(), E> {
        self.write(i2c, Register::POLARITY_INVERT, invert)
    }

    /// The Polarity Inversion registers allow polarity inversion of pins defined as inputs by the
    /// Configuration register. If a bit in this register is set the corresponding pin's polarity
    /// is inverted. If a bit in this register is cleared, the corresponding pin's original polarity
    /// is retained.
    pub fn is_inverted(&self, i2c: &mut T) -> Result<Port, E> {
        self.read(i2c, Register::POLARITY_INVERT)
    }
}

#[allow(non_camel_case_types)]
#[derive(Copy, Clone)]
pub enum Register {
    INPUT_PORT = 0x00,
    OUTPUT_PORT = 0x01,
    POLARITY_INVERT = 0x02,
    CONFIG_PORT = 0x03,
}